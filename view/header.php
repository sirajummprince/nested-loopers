<!DOCTYPE html>

<html>
    <head>
        <title>Amar Proshno</title>
<!--        CSS-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--        <link rel="stylesheet" href="../assets/css/bootstrap.min.css">-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--        <link rel="stylesheet" href="../assets/css/styles.css">-->
<!--        <link rel="stylesheet" href="../assets/css2/styles.css">-->
        <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../assets/js/bootstrap.js">
        <link rel="stylesheet" href="../assets/css/bootstrap.css">
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css.map">
        <link rel="stylesheet" href="../assets/css/bootstrap-theme.css">
<!--        <link rel="stylesheet" href="../assets/css/login.css">-->
<!--        <link rel="stylesheet" href="../assets/css/signUp.css">-->
        <link rel="stylesheet" href="../assets/css/styles.css">

<!--        CSS2-->


        <link rel="stylesheet" href="../assets/css2/Google-Style-Login.css">
        <link rel="stylesheet" href="../assets/css2/Pretty-Footer.css">

        <link rel="stylesheet" href="../assets/css2/Pretty-Header.css">
        <link rel="stylesheet" href="../assets/css2/Pretty-Registration-Form.css">
        <link rel="stylesheet" href="../assets/css2/Pretty-Search-Form.css">
        <link rel="stylesheet" href="../assets/css2/styles.css">
        <link rel="stylesheet" href="../assets/css2/untitled.css">

<!--        FONTS-->
        <link rel="stylesheet" href="../assets/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="../https://fonts.googleapis.com/css?family=Cookie">
        <link rel="stylesheet" href="../assets/fonts/fontawesome-webfont.eot">

    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="../index.php">
                        <!-- <img src="../assets/images/pitanja.png" class="titleimage" height="50" width="60"> -->
                        <span class="titleAmar">amar</span><span class="titleProshno">proshno</span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="question.php">Question</a></li>
                        <li><a href="Question/ask.php">Ask a question</a></li>
                        <li><a href="edit.php">Documentation</a></li>
                        <li><a href="#">Tags</a></li>
                        <li><a href="#">Users</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </form>

                    <ul class="nav navbar-nav singUp ">
                        <li><a href="Auth/login.php"><span class="login">Log In</span></a></li>
                        <li class="btnstyle"><a href="Auth/signUp.php"><button type="button" class="btn btn-primary">Sign Up</button></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

