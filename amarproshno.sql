-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2017 at 08:22 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `amarproshno`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` text NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=2 ;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `answer`, `question_id`, `user_id`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'asdasd', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titles` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `types` varchar(20) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=16 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `titles`, `description`, `types`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
(1, 'abc', '<p>description&nbsp;</p>\r\n', 'type', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'asdasd', '<p>sadasdasd</p>\r\n', 'asdasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'asdasd', '<p>sadasdasd</p>\r\n', 'asdasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'erwer', '<p>rwerf</p>\r\n', 'rwr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'erwer', '<p>rwerf</p>\r\n', 'rwr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'erwer', '<p>rwerf</p>\r\n', 'rwr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'erwer', '<p>rwerf</p>\r\n', 'rwr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'erwer', '<p>rwerf</p>\r\n', 'rwr', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'dasd', '<p>asdasd</p>\r\n', 'sadasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'asdasfasf', '<p>safsafsfsfsafasf</p>\r\n', 'safsfsf', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'asda', '<p>asdad</p>\r\n', 'asdasd', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '<p>aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</p>\r\n', 'aaaaaaaaaaaaaaaaaaaa', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'a', '<p>a</p>\r\n', 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'qq', '<p>qq</p>\r\n', 'qq', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `created_at`) VALUES
(1, 'ayon', 'asasdasd', 'asdasd@sda.comd', '0000-00-00'),
(2, 'asdas', 'sdasad', 'asdasdasdasd@sda.comd', '0000-00-00'),
(3, 'dasd', 'sadsad', 'asdasdasdasd@sda.comd', '0000-00-00'),
(4, 'ads', 'sdasd', 'asdasd@sda.com', '0000-00-00'),
(5, 'aydsad', 'sdasd', 'asdasd@sda.comd', '0000-00-00'),
(6, 'ayon', 'asdasd', 'asdasdasdasd@sda.comd', '0000-00-00'),
(7, 'asdasd', 'asdasd', 'asdasdasdasd@sda.comd', '0000-00-00'),
(8, 'asdasd', 'sad', 'asdasdasdasd@sda.comd', '0000-00-00'),
(9, 'asdasd', 'sad', 'asdasdasdasd@sda.comd', '0000-00-00'),
(10, 'jaja', 'asdasd', 'asdasd@sda.com', '0000-00-00'),
(11, 'asdasd', 'asdasd', 'asdasd@sda.com', '0000-00-00'),
(12, 'asdads', 'sadasd', 'asdasdsupport@company.com', '0000-00-00'),
(13, 'Pizza', 'asdas', 'dasd@fa', '0000-00-00'),
(14, 'Ss', 'ss', 'mirelahi33@gmail.com', '0000-00-00'),
(15, 'a', 'a', 'a', '2017-04-13'),
(16, 'aaaaaaaaaaaaaaaaa', 'b0f0545856af1a340acdedce23c54b97', 'mirelahi33@gmail.com', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `firstName` varchar(20) NOT NULL,
  `middleName` varchar(20) NOT NULL,
  `lastName` varchar(20) NOT NULL,
  `dateOfBirth` datetime NOT NULL,
  `gender` varchar(20) NOT NULL,
  `photo` blob NOT NULL,
  `hobby` varchar(50) NOT NULL,
  `interest` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `deletedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
