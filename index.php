<?php use App\Auth\Auth; ?>


<!DOCTYPE html>

<html>
    <head>
        <title>Amar Proshno</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/styles.css">
         <link rel="stylesheet" href="assets/css2/styles.css">
           <link rel="stylesheet" href="assets/css2/Pretty-Footer.css">
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="index.php">
                     <!--    <img src="assets/images/pitanja.png" class="titleimage" height="50" width="60"> -->
                        <span class="titleAmar">amar</span><span class="titleProshno">proshno</span>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="view/question.php">Question</a></li>
                        <li><a href="view/Question/ask.php">Ask Question</a></li>
                        <li><a href="view/edit.php">Documentation</a></li>
                        <li><a href="#">Tags</a></li>
                        <li><a href="#">Users</a></li>
                    </ul>
                    <form class="navbar-form navbar-left">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
                        </div>
                    </form>

                    <ul class="nav navbar-nav singUp ">
                        <li><a href="view/Auth/login.php"><span class="login">Log In</span></a></li>
                        <li class="btnstyle"><a href="view/Auth/signUp.php"><button type="button" class="btn btn-primary">Sign Up</button></a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header">
                        <p class="JoinTitle">
                            <span>Join the Amar Proshno Community</span>
                        </p>
                        <p class="bottom"></p>
                        <p class="Community">
                            Amar Proshno is a community of million programmers, just like you, helping each other.
                            Join them; it only takes a minute:
                        </p></br>
                        <p>
                           <a href="view/Auth/signUp.php"><button type="submit" class="btn btn-primary btn-lg">Sign Up</button></a>
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="content">
                        <div class="leftContent">
                            <div class="topQue">
                                <p>Top Questions</p>
                            </div>


                                    <div class="QueBox">
                                        <p  class="QueTitle">Hello. How are you gys ?</p>
                                    </div>
                                    <div class="DisBox">
                                        <p class="QueDis">
                                            Sed posuere mi ac ligula malesuada posuere.
                                            Aliquam eget tempus odio. Morbi vel tristique nisi, vitae elementum ante.
                                            Aliquam vitae pretium felis.Pellentesque fermentum et purus ut pulvinar.<a href="#" class="readmore">read more </a>
                                        </p>
                                    </div>
                            <div class="QueBox">
                                <p  class="QueTitle">Hello. How are you gys ?</p>
                            </div>
                            <div class="DisBox">
                                <p class="QueDis">
                                    Sed posuere mi ac ligula malesuada posuere.
                                    Aliquam eget tempus odio. Morbi vel tristique nisi, vitae elementum ante.
                                    Aliquam vitae pretium felis.Pellentesque fermentum et purus ut pulvinar.<a href="#" class="readmore">read more </a>
                                </p>
                            </div>
                            <div class="QueBox">
                                <p  class="QueTitle">Hello. How are you gys ?</p>
                            </div>
                            <div class="DisBox">
                                <p class="QueDis">
                                    Sed posuere mi ac ligula malesuada posuere.
                                    Aliquam eget tempus odio. Morbi vel tristique nisi, vitae elementum ante.
                                    Aliquam vitae pretium felis.Pellentesque fermentum et purus ut pulvinar.<a href="#" class="readmore">read more </a>
                                </p>
                            </div>
                            <div class="QueBox">
                                <p  class="QueTitle">Hello. How are you gys ?</p>
                            </div>
                            <div class="DisBox">
                                <p class="QueDis">
                                    Sed posuere mi ac ligula malesuada posuere.
                                    Aliquam eget tempus odio. Morbi vel tristique nisi, vitae elementum ante.
                                    Aliquam vitae pretium felis.Pellentesque fermentum et purus ut pulvinar.<a href="#" class="readmore">read more </a>
                                </p>
                            </div>
                            <div class="QueBox">
                                <p  class="QueTitle">Hello. How are you gys ?</p>
                            </div>
                            <div class="DisBox">
                                <p class="QueDis">
                                    Sed posuere mi ac ligula malesuada posuere.
                                    Aliquam eget tempus odio. Morbi vel tristique nisi, vitae elementum ante.
                                    Aliquam vitae pretium felis.Pellentesque fermentum et purus ut pulvinar.<a href="#" class="readmore">read more </a>
                                </p>
                            </div>


                                </div>





                        </div>
                        <div class="rightContent">
                            <div class="askQue">
                                <p class="AskBtn"><button type="submit" class="btn btn-primary btnnn" name="submit">Ask Question</button></p>
                            </div>
                            <div class="topQue">
                                <p>Hot Questions</p>
                            </div>
                            <div class="Advertise">

                                <a href="https://www.facebook.com"><h3>how you doing</h3></a>
                                <a href="https://www.facebook.com"><h3>how you doing</h3></a>
                                <a href="https://www.facebook.com"><h3>how you doing</h3></a>
                                <a href="https://www.facebook.com"><h3>how you doing</h3></a>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
       

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </body>
</html>

<?php include "view/footer.php"; ?> 